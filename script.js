let trainer = {
	name: 'Ash Ketchum',
	age: 33,
	pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
	friends: {
		friends1: 'carlo',
		friends2: 'jv',
		otherFriends: ['dexter', 'melmel']
		
	}
}
console.log(trainer)

let talk = trainer.name
console.log(`${talk}! I choose You`)

console.log(trainer.pokemon)
console.log(trainer['friends'])

function pokemon(name, level, health, attack){
	this.name = name;
	this.level = level;
	this.health = 5 * level;
	this.attack = 2 * level;

	this.tackle = function(oponent){

		console.log(`${this.name} attack ${oponent.name}`)

		oponent.health -= this.attack;

		console.log(`${oponent.name} health is now reduced to ${oponent.health}`)	
		
		if(oponent.health <= 0){
			oponent.faint()
		}
		
		}

		this.faint = function(){
		console.log(`${this.name} is fainted`)
		}
}

let bulbasaur = new pokemon("Bulbasaur", 30)
let charizard = new pokemon("Charizard", 100)
let mewtwo = new pokemon("Mewtwo", 50)

bulbasaur.tackle(charizard)
charizard.tackle(bulbasaur)
mewtwo.tackle(charizard)

	







